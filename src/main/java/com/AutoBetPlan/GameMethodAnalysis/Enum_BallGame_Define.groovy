package com.AutoBetPlan.GameMethodAnalysis

public enum Enum_BallGame_Define {


    // Game Category
    GAME_EVERYHOURS("SSC"), GAME_11PICK5("11X5"), GAME_3DP3("3DP3"), GAME_K3("K3"), GAME_KLSF("KLSF"), GAME_PK10(
            "PK10"), GAME_KENO("KENO"),

    GAME_BETTING("betting")

    private String code


    Enum_BallGame_Define(String code) {
        this.code = code
    }

    String getCode() {
        return code
    }

    @Override
     String toString() {
        return this.getCode()
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @return
     */
     static Enum_BallGame_Define getEnumNameByCode(String GameCategory) {
        for (Enum_BallGame_Define v : values())
            if (v.getCode().equalsIgnoreCase(GameCategory)) {

                return v
            }
        return GAME_BETTING
    }

}
