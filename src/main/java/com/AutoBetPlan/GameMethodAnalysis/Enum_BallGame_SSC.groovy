package com.AutoBetPlan.GameMethodAnalysis

import com.AutoBetPlan.GameInfo.Enum_AutoBettingType
import com.AutoBetPlan.GameInfo.Enum_Type_SSC
import com.AutoBetPlan.analysis.Analysis_DanShiNumRandom
import com.AutoBetPlan.beans.API_BetPlanInfo
import com.AutoBetPlan.beans.API_OpenDraw
import com.AutoBetPlan.beans.beans_TaskRecordTask
import com.AutoBetPlan.beans.beans_TaskRecord_BetInfo
import com.AutoBetPlan.beans_AnalysisType.*
import groovy.json.JsonSlurper

/***
 * 时时彩 方案计算中心
 * 依照 方案类型，玩法 和其他 参数 来决定 方案投注的内容
 */
@Singleton
class Enum_BallGame_SSC {
    private API_OpenDraw IssueInfo
    private API_OpenDraw.PeriodListBean currentOpenDrawInfo
    private API_OpenDraw.PeriodListBean LastestOpenDrawInfo
    private String GameType
    private String TaskAnalysisType
    private int TaskBetTimes
    def currentTask = new beans_TaskRecordTask()

    void initData(String GameType, API_OpenDraw IssueInfo, beans_TaskRecordTask currentTask) {
        this.TaskBetTimes = TaskBetTimes
        this.GameType = GameType
        this.IssueInfo = IssueInfo
        this.currentTask = currentTask
        LastestOpenDrawInfo = IssueInfo.periodList.get(IssueInfo.periodList.size() - 2)
        currentOpenDrawInfo = IssueInfo.periodList.get(IssueInfo.periodList.size() - 1)
        TaskAnalysisType = currentTask.getTaskItem_Type()
    }

    /***
     * 更新方案 投注内容
     * @param currentTask
     * @return
     */
    def getBetInfoInCurrentTask() {
        def MethodCode = currentTask.getTaskItem_MethodCode()
        def newBetCode = new beans_TaskRecord_BetInfo()
        newBetCode.with {
            setBrokerage(0)
            setGame(GameType)
            setMethod(MethodCode)
            setOriginalNumbers(calCulatorBetInfo_OraginNumber(MethodCode, TaskAnalysisType))
            setMulti(calCulatorBetInfo_BeiShu())
            setMode(currentTask.getTaskItem_Mode())
            setPeriod(IssueInfo.periodList.get(IssueInfo.periodList.size() - 1).signature)
        }
        return newBetCode
    }

    /**
     * 计算倍投 高级倍投
     * @return
     */
    private int calCulatorBetInfo_BeiShu() {
        String[] multiArray = currentTask.getTaskItem_Multi().split(",")
        int SizeOfUserMulti = multiArray.length

        if (SizeOfUserMulti >= TaskBetTimes) {
            return Integer.valueOf(multiArray[TaskBetTimes])
        } else {
            return Integer.valueOf(multiArray[TaskBetTimes % SizeOfUserMulti])
        }

    }

    /***
     * 依据不同 方案种类 及 玩法 产生投注号码
     * @param singlePlanItem
     * @param MethodCode
     * @param AnalysisType
     */
    private String calCulatorBetInfo_OraginNumber(String MethodCode, String AnalysisType) {
        switch (Enum_AutoBettingType.getEnumNameByCode(AnalysisType)) {
            case Enum_AutoBettingType.TYPE_RANDOM_NUM:
                getAnalysisNum_Random(MethodCode)
                break
            case Enum_AutoBettingType.TYPE_MISSING_NUM:
                getAnalysisNum_Missing(MethodCode)
                break
            case Enum_AutoBettingType.TYPE_KMTM_OPEN:
                getAnalysisNum_KMTM(MethodCode)
                break
            case Enum_AutoBettingType.TYPE_FIX_NUM:
                getAnalysisNum_Fix(MethodCode)
                break
        }
    }
    /***
     * 遺漏開號
     * 根據 前几期开奖开出的 遗漏值 提出号码
     * @param singlePlanItem
     * @param MethodCode
     * @return
     */
    private String getAnalysisNum_Missing(String MethodCode) {
        println("MISSING " + currentTask.getTaskItem_MethodContent().toString())
        def content = currentTask.getTaskItem_MethodContent()

        def JsonObj = new JsonSlurper().parseText(content)
        def MissingNumInfo = new bean_AutoBetting_PlanType_MissingNumJson(JsonObj)

        def typeSimple = MissingNumInfo.type.toString()
        def Number = MissingNumInfo.value
        switch (typeSimple) {
            case "<":
                break
            case "<=":
                break
            case ">":
                break
            case ">=":
                break
            case "=":
                break
        }

        switch (Enum_Type_SSC.getEnumNameByCode(MethodCode)) {
            case Enum_Type_SSC.YiXing_DingWeiDan_W:
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_Q:
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_B:
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_S:
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_G:
                break
        }
    }

    /***
     * 开某投某，根据上一期的 开出位数的奖期
     * 来决定这一期将要开出哪一些号码
     * @param singlePlanItem
     * @param MethodCode
     * @return
     */
    private String getAnalysisNum_KMTM( String MethodCode) {
        def content = currentTask.getTaskItem_MethodContent()
        println("KMTM " + content)

        String LastestOpenNum = LastestOpenDrawInfo.getNumbers()
        String[] openNumArray = LastestOpenNum.split(",")

        def JsonObj = new JsonSlurper().parseText(content)
        def KMTMNumInfo = new bean_AutoBetting_PlanType_KMTMNumJson(JsonObj)

        String type = KMTMNumInfo.type
        ArrayList<bean_AutoBetting_PlanType_KMTMNumJson_Details> DetailsList = KMTMNumInfo.detail

        String OriganNum = ""

        switch (Enum_Type_SSC.getEnumNameByCode(MethodCode)) {
            case Enum_Type_SSC.YiXing_DingWeiDan_W:

                OriganNum = (type == "ZHENG_TOU" ? DetailsList.get(openNumArray[0]).type_zhengTou : DetailsList.get(openNumArray[0]).type_fanTou)

                OriganNum = "" + OriganNum.replace(" ", ",") + "||||"
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_Q:
                OriganNum = (type == "ZHENG_TOU" ? DetailsList.get(openNumArray[1]).type_zhengTou : DetailsList.get(openNumArray[1]).type_fanTou)

                OriganNum = "|" + OriganNum.replace(" ", ",") + "|||"
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_B:
                OriganNum = (type == "ZHENG_TOU" ? DetailsList.get(openNumArray[2]).type_zhengTou : DetailsList.get(openNumArray[2]).type_fanTou)

                OriganNum = "||" + OriganNum.replace(" ", ",") + "||"
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_S:
                OriganNum = (type == "ZHENG_TOU" ? DetailsList.get(openNumArray[3]).type_zhengTou : DetailsList.get(openNumArray[3]).type_fanTou)

                OriganNum = "|||" + OriganNum.replace(" ", ",") + "|"
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_G:
                OriganNum = (type == "ZHENG_TOU" ? DetailsList.get(openNumArray[4]).type_zhengTou : DetailsList.get(openNumArray[4]).type_fanTou)

                OriganNum = "||||" + OriganNum.replace(" ", ",") + ""
                break
        }
        return OriganNum
    }

    /***
     * 随机出号 单式依据 用户需求的位数 来 投注
     * @param singlePlanItem
     * @param MethodCode
     * @return
     */
    private String getAnalysisNum_Random(String MethodCode) {
        def content = currentTask.getTaskItem_MethodContent()
        println("RandomNum_" + content)

        def JsonObj = new JsonSlurper().parseText(content)
        def RandomNumInfo = new bean_AutoBetting_PlanType_RandomNumJson(JsonObj)

        def RandomTotalAmount = RandomNumInfo.numbers

        String OriganNum = ""
        Analysis_DanShiNumRandom rand = new Analysis_DanShiNumRandom()
        switch (Enum_Type_SSC.getEnumNameByCode(MethodCode)) {
            case Enum_Type_SSC.ZhongSan_ZhiXuanDanShi:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 3)
                break
            case Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 2)
                break
            case Enum_Type_SSC.ErXing_QianErDanShi_ZhiXuan:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 2)
                break
            case Enum_Type_SSC.WuXing_DanShi:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 5)
                break
            case Enum_Type_SSC.QianSan_ZhiXuanDanShi:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 3)
                break
            case Enum_Type_SSC.HouSan_ZhiXuanDanShi:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 3)
                break
            case Enum_Type_SSC.SiXing_ZhiXuanDanShi:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 4)
                break
            default:
                break
        }
        return OriganNum
    }

    /***
     * 固定开号，每一期 依据 用户号码 去 开号
     * @param singlePlanItem
     * @return
     */
    private String getAnalysisNum_Fix(String MethodCode) {
        def content = currentTask.getTaskItem_MethodContent()
        println("FIX_Num"+content)

        def JsonObj = new JsonSlurper().parseText(content)
        def FixNumInfo = new bean_AutoBetting_PlanType_FixNumJson(JsonObj)

        return FixNumInfo.numbers
    }

}
