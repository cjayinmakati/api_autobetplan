package com.AutoBetPlan.beans

/**
 * Created by cjay on 2017/3/21.
 */
class beans_TaskRecord_BetInfo {
    boolean isUpdate  //是否已更新 最新投注
    //以下参数完全跟 网页投注一样
    String originalNumbers
    String game
    String period
    String method
    int multi
    int mode
    int brokerage
}
