package com.AutoBetPlan.beans;

import java.util.List;

/**
 * Created by cjay on 2017/3/21.
 */
public class API_UpdateBetRecord {

        /**
         * amount : 20000
         * bigGroup : 后三
         * brokerage : 2.5
         * code : 1b41171d
         * createIp : 203.190.74.200
         * finalPrize : 0
         * gameName : 重庆时时彩
         * gameType :
         * hardware :
         * id : 1403586
         * level : 1959
         * methodCode : ##3DC
         * methodName : 直选复式
         * multiple : 1
         * numbers : 0,8,5,3,2
         * orderTime : 2017-03-16 17:31:37
         * originalNumbers : 4;9;4
         * personid : 0
         * price : 20000
         * prize : 19540000
         * qty : 1
         * signature : 20170316070
         * smallGroup : 直选
         * status : SEALED
         * type : CQSSC
         * username : androidtest
         * won : false
         * wonQty : 0
         * IsFromTask : true
         * TaskIsActive : true
         * TaskStatus : true
         * TaskList_ItemInfoList : {"TaskName":"掛機","TaskCode":"v1rrwrrb","TaskType":"MISS_NUMBER","TaskStartTime":"2017-01-01 04:05","TaskEndTime":"2017-01-01 05:05","TaskMulti":"1,2,3,4,5"}
         */

        private int amount;
        private String bigGroup;
        private double brokerage;
        private String code;
        private String createIp;
        private int finalPrize;
        private String gameName;
        private String gameType;
        private String hardware;
        private int id;
        private int level;
        private String methodCode;
        private String methodName;
        private int multiple;
        private String numbers;
        private String orderTime;
        private String originalNumbers;
        private int personid;
        private int price;
        private int prize;
        private int qty;
        private String signature;
        private String smallGroup;
        private String status;
        private String type;
        private String username;
        private boolean won;
        private int wonQty;
        private boolean IsFromTask;
        private boolean TaskIsActive;
        private boolean TaskStatus;
        private TaskList_ItemInfoListBean TaskList_ItemInfoList;

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getBigGroup() {
            return bigGroup;
        }

        public void setBigGroup(String bigGroup) {
            this.bigGroup = bigGroup;
        }

        public double getBrokerage() {
            return brokerage;
        }

        public void setBrokerage(double brokerage) {
            this.brokerage = brokerage;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCreateIp() {
            return createIp;
        }

        public void setCreateIp(String createIp) {
            this.createIp = createIp;
        }

        public int getFinalPrize() {
            return finalPrize;
        }

        public void setFinalPrize(int finalPrize) {
            this.finalPrize = finalPrize;
        }

        public String getGameName() {
            return gameName;
        }

        public void setGameName(String gameName) {
            this.gameName = gameName;
        }

        public String getGameType() {
            return gameType;
        }

        public void setGameType(String gameType) {
            this.gameType = gameType;
        }

        public String getHardware() {
            return hardware;
        }

        public void setHardware(String hardware) {
            this.hardware = hardware;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public String getMethodCode() {
            return methodCode;
        }

        public void setMethodCode(String methodCode) {
            this.methodCode = methodCode;
        }

        public String getMethodName() {
            return methodName;
        }

        public void setMethodName(String methodName) {
            this.methodName = methodName;
        }

        public int getMultiple() {
            return multiple;
        }

        public void setMultiple(int multiple) {
            this.multiple = multiple;
        }

        public String getNumbers() {
            return numbers;
        }

        public void setNumbers(String numbers) {
            this.numbers = numbers;
        }

        public String getOrderTime() {
            return orderTime;
        }

        public void setOrderTime(String orderTime) {
            this.orderTime = orderTime;
        }

        public String getOriginalNumbers() {
            return originalNumbers;
        }

        public void setOriginalNumbers(String originalNumbers) {
            this.originalNumbers = originalNumbers;
        }

        public int getPersonid() {
            return personid;
        }

        public void setPersonid(int personid) {
            this.personid = personid;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getPrize() {
            return prize;
        }

        public void setPrize(int prize) {
            this.prize = prize;
        }

        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
        }

        public String getSignature() {
            return signature;
        }

        public void setSignature(String signature) {
            this.signature = signature;
        }

        public String getSmallGroup() {
            return smallGroup;
        }

        public void setSmallGroup(String smallGroup) {
            this.smallGroup = smallGroup;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public boolean isWon() {
            return won;
        }

        public void setWon(boolean won) {
            this.won = won;
        }

        public int getWonQty() {
            return wonQty;
        }

        public void setWonQty(int wonQty) {
            this.wonQty = wonQty;
        }

        public boolean isIsFromTask() {
            return IsFromTask;
        }

        public void setIsFromTask(boolean IsFromTask) {
            this.IsFromTask = IsFromTask;
        }

        public boolean isTaskIsActive() {
            return TaskIsActive;
        }

        public void setTaskIsActive(boolean TaskIsActive) {
            this.TaskIsActive = TaskIsActive;
        }

        public boolean isTaskStatus() {
            return TaskStatus;
        }

        public void setTaskStatus(boolean TaskStatus) {
            this.TaskStatus = TaskStatus;
        }

        public TaskList_ItemInfoListBean getTaskList_ItemInfoList() {
            return TaskList_ItemInfoList;
        }

        public void setTaskList_ItemInfoList(TaskList_ItemInfoListBean TaskList_ItemInfoList) {
            this.TaskList_ItemInfoList = TaskList_ItemInfoList;
        }

        public static class TaskList_ItemInfoListBean {
            /**
             * TaskName : 掛機
             * TaskCode : v1rrwrrb
             * TaskType : MISS_NUMBER
             * TaskStartTime : 2017-01-01 04:05
             * TaskEndTime : 2017-01-01 05:05
             * TaskMulti : 1,2,3,4,5
             */

            private String TaskName;
            private String TaskCode;
            private String TaskType;
            private String TaskStartTime;
            private String TaskEndTime;
            private String TaskMulti;

            public String getTaskName() {
                return TaskName;
            }

            public void setTaskName(String TaskName) {
                this.TaskName = TaskName;
            }

            public String getTaskCode() {
                return TaskCode;
            }

            public void setTaskCode(String TaskCode) {
                this.TaskCode = TaskCode;
            }

            public String getTaskType() {
                return TaskType;
            }

            public void setTaskType(String TaskType) {
                this.TaskType = TaskType;
            }

            public String getTaskStartTime() {
                return TaskStartTime;
            }

            public void setTaskStartTime(String TaskStartTime) {
                this.TaskStartTime = TaskStartTime;
            }

            public String getTaskEndTime() {
                return TaskEndTime;
            }

            public void setTaskEndTime(String TaskEndTime) {
                this.TaskEndTime = TaskEndTime;
            }

            public String getTaskMulti() {
                return TaskMulti;
            }

            public void setTaskMulti(String TaskMulti) {
                this.TaskMulti = TaskMulti;
            }
        }

}
