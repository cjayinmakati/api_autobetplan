package com.AutoBetPlan.GameInfo;

public enum Enum_Type_K3 {
	SanBuTongHao("K3BT3"), SanTongHaoDanXuan("K3DX"), SanTongHaoTongXuan("K3S3"), SanLianHao("K3LH3"), ErBuTongHao(
			"K3BT2"), ErTongHaoDanXuan("K3DX2"), ErTongHaoFuXuan(
					"K3FX2"), HeZhi("K3DT"), CaiDanShuang("K3GO"), CaiYiGeHaoJiuZhongJiang("K3G1"), CaiDaXiao("K3GB");

	private String code;

	Enum_Type_K3(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	@Override
	public String toString() {
		return this.getCode();
	}

	/***
	 * 用code找名字 并核对返回到
	 * 
	 * @param MethodCode
	 * @return
	 */

	public static Enum_Type_K3 getEnumNameByCode(String MethodCode) {

		for (Enum_Type_K3 v : values())
			if (v.getCode().equalsIgnoreCase(MethodCode)) {
				return v;
			}
		return Enum_Type_K3.HeZhi;

	}
}
