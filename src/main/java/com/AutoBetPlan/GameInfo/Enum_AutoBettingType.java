package com.AutoBetPlan.GameInfo;

/***
 * 方案种类
 */
public enum Enum_AutoBettingType {
    TYPE_RANDOM_NUM("随机出号", "RANDOM_NUMBER"),
    TYPE_MISSING_NUM("遗漏出号", "MISS_NUMBER"),
    TYPE_KMTM_OPEN("开某投某", "KMTM_NUMBER"),
    TYPE_FIX_NUM("固定选号", "FIX_NUMBER"),
    TYPE_ANALYSIS_PLAN("外接计画", "ANALYSIS_PLAN");


    private String Code;
    private String Name;
    Enum_AutoBettingType(String Name, String Code) {
        this.Code = Code;
        this.Name = Name;
    }

    public String getCode() {
        return Code;
    }

    public String getName() {
        return Name;
    }


    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    public static Enum_AutoBettingType getEnumNameByCode(String MethodCode) {

        for (Enum_AutoBettingType v : values())
            if (v.getCode().equalsIgnoreCase(MethodCode)) {
                return v;
            }

        return TYPE_RANDOM_NUM;

    }
}
