package com.AutoBetPlan.GameInfo;

public enum Enum_Type_KENO {

    RenXuanYi("1O1C"), RenXuanEr("1O2C"), RenXuanSan("1O3C"), RenXuanSi("1O4C"), RenXuanWu("1O5C"), RenXuanLiu(
            "1O6C"), RenXuanQi("1O7C"),

    ShangXiaPan("K8UMD"), QiOuPan("K8OTE"), HeZhiDaXiaoDanShuang("K8GBO");

    private String code;

    Enum_Type_KENO(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return this.getCode();
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    public static Enum_Type_KENO getEnumNameByCode(String MethodCode) {
        for (Enum_Type_KENO v : values()) {
            if (v.getCode().equalsIgnoreCase(MethodCode)) {
                return v;
            }
        }
        return RenXuanYi;
    }
}
