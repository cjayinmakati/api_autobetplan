package com.AutoBetPlan.GameInfo;


public enum Enum_Type_SSC {

    YiXing_DingWeiDan_W("1DP_W"),
    YiXing_DingWeiDan_Q("1DP_Q"),
    YiXing_DingWeiDan_B("1DP_B"),
    YiXing_DingWeiDan_S("1DP_S"),
    YiXing_DingWeiDan_G("1DP_G"),

    // 不定位
    BuDingWei_HouSanYiMaBuDingWei("*3F1"),
    BuDingWei_HouSanErMaBuDingWei("*3F2"),
    BuDingWei_QianSanYiMaBuDingWei("3*F1"),
    BuDingWei_QianSanErMaBuDingWei("3*F2"),
    BuDingWei_SiXingYiMaBuDingWei("4F1"),
    BuDingWei_SiXingErMaBuDingWei("4F2"),
    BuDingWei_WuXingErMaBuDingWei("5F2"),
    BuDingWei_WuXingSanMaBuDingWei("5F3"),

    // 中三
    ZhongSan_ZhiXuanFuShi("#3#DC"),
    ZhongSan_KuaDu("#3#DD"),
    ZhongSan_ZhiXuanDanShi("#3#DS"),
    ZhongSan_ZhiXuanHeZhi("#3#DT"),
    ZhongSan_ZuSan("#3#G3"),
    ZhongSan_ZuLiu("#3#G6"),
    ZhongSan_BaoDan("#3#GA"),
    ZhongSan_HunHeZuXuan("#3#GM"),
    ZhongSan_TeShuHaoMa("#3#GP"),
    ZhongSan_ZuXuanHeZhi("#3#GT"),

    ErXing_HouErFuShi_ZhiXuan("*2DC"),
    ErXing_HouErKuaDu_ZhiXuan("*2DD"),
    ErXing_HouErDanShi_ZhiXuan("*2DS"),
    ErXing_HouErHeZhi_ZhiXuan("*2DT"),

    ErXing_HouErBaoDan_ZuXuan("*2GA"),
    ErXing_HouErFuShi_ZuXuan("*2GC"),
    ErXing_HouErDanShi_ZuXuan("*2GS"),
    ErXing_HouErHeZhi_ZuXuan("*2GT"),

    ErXing_QianErFuShi_ZhiXuan("2*DC"),
    ErXing_QianErKuaDu_ZhiXuan("2*DD"),
    ErXing_QianErDanShi_ZhiXuan("2*DS"),
    ErXing_QianErHeZhi_ZhiXuan("2*DT"),
    ErXing_QianErBaoDan_ZuXuan("2*GA"),
    ErXing_QianErFuShi_ZuXuan("2*GC"),
    ErXing_QianErDanShi_ZuXuan("2*GS"),
    ErXing_QianErHeZhi_ZuXuan("2*GT"),

    // 五星
    WuXing_FuShi("5DC"),
    WuXing_DanShi("5DS"),
    WuXing_ZuXuan10("5G10"),
    WuXing_ZuXuan120("5G120"),
    WuXing_ZuXuan20("5G20"),
    WuXing_ZuXuan30("5G30"),
    WuXing_ZuXuan5("5G5"),
    WuXing_ZuXuan60("5G60"),

    // 任選3
    RenXuanSan_ZhiXuanFuShi("A3DC"),
    RenXuanSan_ZhiXuanDanShi("A3DS"),
    RenXuanSan_ZhiXuanHeZhi("A3DT"),
    RenXuanSan_ZuSanFuShi("A3G3C"),
    RenXuanSan_ZuSanDanShi("A3G3S"),
    RenXuanSan_ZuLiuFuShi("A3G6C"),
    RenXuanSan_ZuLiuDanShi("A3G6S"),
    RenXuanSan_HunHeZuXuan("A3GM"),
    RenXuanSan_ZuXuanHeZhi("A3GT"),

    // 任選2
    RenXuanEr_ZhiXuanFuShi("A2DC"),
    RenXuanEr_ZhiXuanDanShi("A2DS"),
    RenXuanEr_ZhiXuanHeZhi("A2DT"),
    RenXuanEr_ZuXuanFuShi("A2GC"),
    RenXuanEr_ZuXuanDanShi("A2GS"),
    RenXuanEr_ZuXuanHeZhi("A2GT"),

    // 任選4
    RenXuanSi_ZhiXuanFuShi("A4DC"),
    RenXuanSi_ZhiXuanDanShi("A4DS"),
    RenXuanSi_ZuXuan12("A4G12"),
    RenXuanSi_ZuXuan24("A4G24"),
    RenXuanSi_ZuXuan4("A4G4"),
    RenXuanSi_ZuXuan6("A4G6"),

    // 前三
    QianSan_ZhiXuanFuShi("3##DC"),
    QianSan_KuaDu("3##DD"),
    QianSan_ZhiXuanDanShi("3##DS"),
    QianSan_ZhiXuanHeZhi("3##DT"),
    QianSan_ZuSan("3##G3"),
    QianSan_ZuLiu("3##G6"),
    QianSan_BaoDan("3##GA"),
    QianSan_HunHeZuXuan("3##GM"),
    QianSan_TeShuHaoMa("3##GP"),
    QianSan_ZuXuanHeZhi("3##GT"),

    // 後三
    HouSan_ZhiXuanFuShi("##3DC"),
    HouSan_KuaDu("##3DD"),
    HouSan_ZhiXuanDanShi("##3DS"),
    HouSan_ZhiXuanHeZhi("##3DT"),
    HouSan_ZuSan("##3G3"),
    HouSan_ZuLiu("##3G6"),
    HouSan_BaoDan("##3GA"),
    HouSan_HunHeZuXuan("##3GM"),
    HouSan_TeShuHaoMa("##3GP"),
    HouSan_ZuXuanHeZhi("##3GT"),
    // 四星
    SiXing_ZhiXuanFuShi("4DC"),
    SiXing_ZhiXuanDanShi("4DS"),
    SiXing_ZuXuan12("4G12"),
    SiXing_ZuXuan24("4G24"),
    SiXing_ZuXuan4("4G4"),
    SiXing_ZuXuan6("4G6"),

    DaXiaoDanShuang_HouErDaXiaoDanShuang("*2S"),
    DaXiaoDanShuang_QianErDaXiaoDanShuang("2*S"),

    QuWei_YiFanFengShun("S1"),
    QuWei_HaoShiChengShuang("S2"),
    QuWei_SanXingBaoXi("S3"),
    QuWei_SiJiFaCai("S4"),

    LongHuHe_WanQian("##DAT"),
    LongHuHe_WanBai("#D#AT"),
    LongHuHe_WanShi("#DA#T"),
    LongHuHe_WanGe("#DAT#"),
    LongHuHe_QianBai("D##AT"),
    LongHuHe_QianShi("D#A#T"),
    LongHuHe_QianGe("D#AT#"),
    LongHuHe_BaiShi("DA##T"),
    LongHuHe_BaiGe("DA#T#"),
    LongHuHe_ShiGe("DAT##");

    private String code;

    Enum_Type_SSC(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return this.getCode();
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    public static Enum_Type_SSC getEnumNameByCode(String MethodCode) {

        for (Enum_Type_SSC v : values())
            if (v.getCode().equalsIgnoreCase(MethodCode)) {
                return v;
            }
        return WuXing_FuShi;
    }

}
