package com.AutoBetPlan.GameInfo;


public enum Enum_Type_PK10 {
    YiXing_DingWeiDan_QianWu("DP1"), YiXing_DingWeiDan_HouWu("DP2"), // 网页上的是DP

    QianSan_DanShi("3*DS"), QianSan_FuShi("3*EDC"),

    QianEr_DanShi("2*DS"), QianEr_FuShi("2*EDC"), QianEr_DaXiaoDanShuang("2*GBO"),

    CaiDanShuang_CaiDanShuang1("GO1"), CaiDanShuang_CaiDanShuang2("GO2"), CaiDanShuang_CaiDanShuang3("GO3"),

    CaiDaXiao_CaiDaXiao1("GB1"), CaiDaXiao_CaiDaXiao2("GB2"), CaiDaXiao_CaiDaXiao3("GB3"),

    ShouWei_FuShi("1*DC");

    private String code;

    Enum_Type_PK10(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return this.getCode();
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    public static Enum_Type_PK10 getEnumNameByCode(String MethodCode) {
        for (Enum_Type_PK10 v : values())
            if (v.getCode().equalsIgnoreCase(MethodCode)) {
                return v;
            }
        return ShouWei_FuShi;
    }
}
