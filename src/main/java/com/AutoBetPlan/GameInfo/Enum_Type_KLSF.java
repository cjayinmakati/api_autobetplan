package com.AutoBetPlan.GameInfo;

public enum Enum_Type_KLSF {

    YiXing_DingWeiDan("1DP"),

    SanXingQianSan_ZhiXuanFuShi("3*DC"), SanXingQianSan_ZuXuanFuShi("3*GC"),

    SanXingHouSan_ZhiXuanFuShi("*3DC"), SanXingHouSan_ZuXuanFuShi("*3GC"),

    ErMa_ErLianZhi("B2DC"), ErMa_ErLianZu("B2GC"),

    RenXuanFuShi_RenXuanYi("1O1C"), RenXuanFuShi_RenXuanEr("2O2C"), RenXuanFuShi_RenXuanSan(
            "3O3C"), RenXuanFuShi_RenXuanSi("4O4C"), RenXuanFuShi_RenXuanWu("5O5C"),

    RenXuanDanTuo_RenXuanErDanTuo("2O2K"), RenXuanDanTuo_RenXuanSanDanTuo("3O3K"), RenXuanDanTuo_RenXuanSiDanTuo(
            "4O4K"), RenXuanDanTuo_RenXuanWuDanTuo("5O5K");

    private String code;

    Enum_Type_KLSF(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return this.getCode();
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    public static Enum_Type_KLSF getEnumNameByCode(String MethodCode) {
        for (Enum_Type_KLSF v : values()) {
            if (v.getCode().equalsIgnoreCase(MethodCode)) {
                return v;
            }
        }
        return YiXing_DingWeiDan;
    }
}
