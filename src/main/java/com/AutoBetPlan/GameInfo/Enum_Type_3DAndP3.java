package com.AutoBetPlan.GameInfo;


public enum Enum_Type_3DAndP3 {

    YiXing_DingWeiDan("1DP"),

    SanMa_FuShi("3DC"), SanMa_DanShi("3DS"), SanMa_HeZhi("3DT"), SanMa_ZuSan("3G3"), SanMa_ZuLiu(
            "3G6"), SanMa_HunHeZuXuan("3GM"),

    BuDingWei_YiMaBuDingWei("3F1"), BuDingWei_ErMaBuDingWei("3F2"),

    QianEr_ZhiXuan_FuShi("2*DC"), QianEr_ZhiXuan_DanShi("2*DS"), QianEr_ZhiXuan_HeZhi("2*DT"),

    QianEr_ZuXuan_FuShi("2*GC"), QianEr_ZuXuan_DanShi("2*GS"), QianEr_ZuXuan_HeZhi("2*GT"),

    HouEr_ZhiXuan_FuShi("*2DC"), HouEr_ZhiXuan_DanShi("*2DS"), HouEr_ZhiXuan_HeZhi("*2DT"),

    HouEr_ZuXuan_FuShi("*2GC"), HouEr_ZuXuan_DanShi("*2GS"), HouEr_ZuXuan_HeZhi("*2GT"),

    DaXiaoDanShuang_HouErDaXiaoDanShuang("*2S"), DaXiaoDanShuang_QianErDaXiaoDanShuang(
            "2*S"), DaXiaoDanShuang_SanMaDaXiaoDanShuang("3S");

    private String code;

    Enum_Type_3DAndP3(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return this.getCode();
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    public static Enum_Type_3DAndP3 getEnumNameByCode(String MethodCode) {
        for (Enum_Type_3DAndP3 v : values())
            if (v.getCode().equalsIgnoreCase(MethodCode)) {
                return v;
            }
        return YiXing_DingWeiDan;

    }
}
