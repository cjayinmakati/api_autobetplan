package com.AutoBetPlan.GameInfo;


public enum Enum_Type_11Pick5 {

    BuDingWei_HouSanBuDingWei("*3F"), BuDingWei_QianSanBuDingWei("F"),

    ErMa_QianErZhiXuanDanShi("2*DS"), ErMa_QianErZhiXuanFuShi("2*EDC"), ErMa_QianErZuXuanFuShi(
            "2*GC"), ErMa_QianErZuXuanDanTuo("2*GK"), ErMa_QianErZuXuanDanShi("2*GS"),

    RenXuanDanShi_RenXuanYiZhongYi("1O1S"), RenXuanDanShi_RenXuanErZhongEr("2O2S"), RenXuanDanShi_RenXuanSanZhongSan(
            "3O3S"), RenXuanDanShi_RenXuanSiZhongSi("4O4S"), RenXuanDanShi_RenXuanWuZhongWu(
            "5O5S"), RenXuanDanShi_RenXuanLiuZhongWu(
            "5O6S"), RenXuanDanShi_RenXuanQiZhongWu("5O7S"), RenXuanDanShi_RenXuanBaZhongWu("5O8S"),

    RenXuanFuShi_RenXuanYiZhongYi("1O1C"), RenXuanFuShi_RenXuanErZhongEr("2O2C"), RenXuanFuShi_RenXuanSanZhongSan(
            "3O3C"), RenXuanFuShi_RenXuanSiZhongSi("4O4C"), RenXuanFuShi_RenXuanWuZhongWu(
            "5O5C"), RenXuanFuShi_RenXuanLiuZhongWu(
            "5O6C"), RenXuanFuShi_RenXuanQiZhongWu("5O7C"), RenXuanFuShi_RenXuanBaZhongWu("5O8C"),

    RenXuanDanTuo_RenXuanErZhongEr("2O2K"), RenXuanDanTuo_RenXuanSanZhongSan("3O3K"), RenXuanDanTuo_RenXuanSiZhongSi(
            "4O4K"), RenXuanDanTuo_RenXuanWuZhongWu("5O5K"), RenXuanDanTuo_RenXuanLiuZhongWu(
            "5O6K"), RenXuanDanTuo_RenXuanQiZhongWu("5O7K"), RenXuanDanTuo_RenXuanBaZhongWu("5O8K"),

    QianSan_QianSanZhiXuanDanShi("3##DS"), QianSan_QianSanZhiXuanFuShi("3##EDC"), QianSan_QianSanZuXuanFuShi(
            "3##GC"), QianSan_QianSanZuXuanDanTuo("3##GK"), QianSan_QianSanZuXuanDanShi("3##GS"),

    HouSan_HouSanZhiXuanDanShi("##3DS"), HouSan_HouSanZhiXuanFuShi("##3EDC"), HouSan_HouSanZuXuanFuShi(
            "##3GC"), HouSan_HouSanZuXuanDanTuo("##3GK"), HouSan_HouSanZuXuanDanShi("##3GS"),

    DingWeiDan_DingWeiDan("DP"),

    QuWeiXing_CaiZhongWei("M"), QuWeiXing_DingDanShuang("S");
    private String code;


    Enum_Type_11Pick5(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return this.getCode();
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    public static Enum_Type_11Pick5 getEnumNameByCode(String MethodCode) {
        for (Enum_Type_11Pick5 v : values())
            if (v.getCode().equalsIgnoreCase(MethodCode)) {
                return v;
            }
        return BuDingWei_HouSanBuDingWei;

    }
}
