package com.AutoBetPlan.Main

import com.AutoBetPlan.beans.API_BetPlanInfo
import com.AutoBetPlan.beans.API_OpenDraw
import com.AutoBetPlan.beans.beans_TaskRecordList
import com.google.gson.Gson

/**
 * Created by cjay on 2017/3/21.
 */

@Singleton
class Entrance {
    Map<String, API_BetPlanInfo> userPlanInfoMap = new HashMap<>()
    Map<String, API_OpenDraw> openDrawList = new HashMap<>()
    Map<String, beans_TaskRecordList> AllUserTaskInfo = new HashMap<>()
    private static final int TIMER_DELAY = 0
    private static final int TIMER_PERIOD = 500

    static void main(args) {
        def BetInfo = """{"userid":"111","username":"androidtest","gameCategory":"SSC","gameType":"FFC1","startTime":"2017-03-23 12:00:00","endTime":"2017-03-24 20:00:00","autoPlanData":[{"planCode":"","methodCode":"1DP","content":"{\\"type\\":\\">\\",\\"value\\":5}","type":"MISS_NUMBER","name":"遗漏方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP","content":"{\\"type\\":\\"ZHENG_TOU\\",\\"detail\\":[{\\"number\\":0,\\"fanTou\\":\\"67890\\",\\"zhengTou\\":\\"12345\\"},{\\"number\\":1,\\"fanTou\\":\\"78901\\",\\"zhengTou\\":\\"23456\\"},{\\"number\\":2,\\"fanTou\\":\\"89012\\",\\"zhengTou\\":\\"34567\\"},{\\"number\\":3,\\"fanTou\\":\\"90123\\",\\"zhengTou\\":\\"45678\\"},{\\"number\\":4,\\"fanTou\\":\\"01234\\",\\"zhengTou\\":\\"56789\\"},{\\"number\\":5,\\"fanTou\\":\\"12345\\",\\"zhengTou\\":\\"67890\\"},{\\"number\\":6,\\"fanTou\\":\\"23456\\",\\"zhengTou\\":\\"78901\\"},{\\"number\\":7,\\"fanTou\\":\\"34567\\",\\"zhengTou\\":\\"89012\\"},{\\"number\\":8,\\"fanTou\\":\\"45678\\",\\"zhengTou\\":\\"90123\\"},{\\"number\\":9,\\"fanTou\\":\\"56789\\",\\"zhengTou\\":\\"01234\\"}]}","type":"KMTM_NUMBER","name":"開某开某投某方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP","content":"{\\"numbers\\":\\"1234233431313257\\"}","type":"FIX_NUMBER","name":"固定方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000},{"planCode":"","methodCode":"##1DP","content":"{\\"numbers\\":100}","type":"RANDOM_NUMBER","name":"隨機方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000}]}"""
        def OpenDrawA = """{"gameId":1,"name":"重慶時時彩","category":"SSC","offSaleAdvance":10,"type":"CQSSC","defaultMethodName":"直选复式","defaultMethodCode":"##3DC","periodList":[{"signature":"20170323949","numbers":"4,3,3,5,1","nowtime":"2017-03-23 10:00:00","openTime":"2017-03-23 15:49:00","statuscode":2},{"signature":"20170323950","numbers":"1,4,7,3,2","nowtime":"2017-03-23 10:00:00","openTime":"2017-03-23 14:00:00","statuscode":0},{"signature":"20170323951","numbers":null,"nowtime":"2017-03-23 13:30:41","openTime":"2017-03-23 16:51:00","statuscode":0}]}"""
        def OpenDrawB = """{"gameId":5,"name":"1分時時彩","category":"SSC","offSaleAdvance":10,"type":"FFC1","defaultMethodName":"直选复式","defaultMethodCode":"##3DC","periodList":[{"signature":"20170323001","numbers":"6,9,2,4,3","nowtime":"2017-03-21 15:49:41","openTime":"2017-03-21 15:49:00","statuscode":2},{"signature":"20170323002","numbers":"5,3,9,2,5","nowtime":"2017-03-21 15:49:41","openTime":"2017-03-21 14:00:00","statuscode":1},{"signature":"20170323003","numbers":null,"nowtime":"2017-03-21 15:49:41","openTime":"2017-03-21 15:51:00","statuscode":0}]}"""
        Gson gs = new Gson()
        API_OpenDraw openDrawItemA = gs.fromJson(OpenDrawA, API_OpenDraw.class)
        API_OpenDraw openDrawItemB = gs.fromJson(OpenDrawB, API_OpenDraw.class)
        API_BetPlanInfo userPlanInfoMapItem = gs.fromJson(BetInfo, API_BetPlanInfo.class)

        Map<String, API_OpenDraw> testData_opendraw = new HashMap<>()
        testData_opendraw.put("FFC1", openDrawItemA)
        testData_opendraw.put("CQSSC", openDrawItemB)

        Map<String, API_BetPlanInfo> testData_userPlanInfoMap = new HashMap<>()
        testData_userPlanInfoMap.put(userPlanInfoMapItem.getUserid(), userPlanInfoMapItem)


        def en = Entrance.instance
        en.initAllData(testData_opendraw, testData_userPlanInfoMap)
        en.createUserBetMap()

        def b = Entrance.instance
        b.initAllData(testData_opendraw, testData_userPlanInfoMap)
        b.createUserBetMap()

        Map<String, beans_TaskRecordList> AllUserTaskInfo = en.getUserBetInfoMap()
        println(AllUserTaskInfo)
    }

    /***
     * 初始化 所有资料
     * @param opendrawMap 奖期资料
     * @param userPlanInfoMap 投注资料
     */
    void initAllData(Map<String, API_OpenDraw> opendrawMap, Map<String, API_BetPlanInfo> userPlanInfoMap) {
        setOpenDrawInfo(opendrawMap)
        setBetTaskFromUser(userPlanInfoMap)
    }

    /***
     * ---------------------------------------------------挂机 任务 新增修改区---------------------------------------------------
     */
    /***
     *  设定 目前用户 掛單任务
     */
    void setBetTaskFromUser(Map<String, API_BetPlanInfo> userPlanInfoMap) {
        this.userPlanInfoMap = userPlanInfoMap
    }

    /***
     * 取得 所有用户 掛單任务
     */
    Map<String, API_BetPlanInfo> getBetTaskFromUser() {
        return userPlanInfoMap
    }

    /***
     * 更改 單一用户 掛單任务
     */
    void setBetTaskFromUser_Singal(API_BetPlanInfo newPlanInfo) {
        userPlanInfoMap.put(newPlanInfo.getUserid(), newPlanInfo)
    }

    /***
     * 取得 单一 用户 掛單任务
     */
    API_BetPlanInfo getBetTaskFromUser_Singal(String userID) {
        return userPlanInfoMap.get(userID)
    }

    /***
     *  强制停止 所有用户 掛單任务
     */
    void setBetTaskFromUser() {
        this.userPlanInfoMap.clear()
    }
    /***
     *  强制停止 该用户 掛單任务 （当 时间 到时 或是 盈亏 达到 最高 或 最低标 时，请传入并停止该用户的 挂机单)
     */
    void stopBetTaskFromUser_Singal(String userID) {
        userPlanInfoMap.remove(userID)
    }

    /***
     * ---------------------------------------------------所有彩种 奖期资讯接口 更新区---------------------------------------------------
     */
    /***
     * 更新 所有彩种 奖期资讯接口
     * **************************(当开奖后 请立即更新该接口)**************************
     */
    void setOpenDrawInfo(Map<String, API_OpenDraw> openDrawList) {
        this.openDrawList = openDrawList
    }

    /***
     * 更新 彩种 开奖 奖期资讯接口
     * **************************(当开奖后 请立即更新该接口)**************************
     */
    void setOpenDrawInfo_Singal(API_OpenDraw opendrawItem) {
        this.openDrawList.put(opendrawItem.getType(), opendrawItem)
    }

    /***
     * ---------------------------------------------------取得 用户挂机 分析出 投注注单 MAP---------------------------------------------------
     */

    /***
     * 请在当前 奖期 截止前 取得用户挂机资讯， 并 依照里面的 模式投注
     * MAP KEY = USER ID
     * MAP VALUE = USER INFO
     */
    Map<String, beans_TaskRecordList> getUserBetInfoMap() {
        return AllUserTaskInfo
    }

    /***
     * 依照用户的 挂机资讯，开奖奖期 去分析并生成 用户当期 注单
     */
    void createUserBetMap() {
        if (userPlanInfoMap) {
            userPlanInfoMap.each { userID, userInfo ->
                API_OpenDraw openDrawInfo = openDrawList.get(userInfo.getGameType())
                API_OpenDraw.PeriodListBean openDrawPeriodInfo = openDrawInfo.getPeriodList().get(openDrawInfo.getPeriodList().size() - 1)

                Date currentTime = new Date()
                Date openTime = Date.parse("YYYY-MM-DD HH:MM", openDrawPeriodInfo.getOpenTime())
                Date startTime = Date.parse("YYYY-MM-DD HH:MM", userInfo.startTime)
                Date endTime = Date.parse("YYYY-MM-DD HH:MM", userInfo.endTime)

                println(currentTime.toString())
                println(openTime.toString())
                println(startTime.toString())
                println(endTime.toString())
                println(openTime.after(startTime))
                println(openTime.before(endTime))

                if (currentTime.after(startTime) && currentTime.before(endTime)) {
                    if (!AllUserTaskInfo.get(userID)) {
                        /**
                         * 剛開始，尚無投注記錄，写入挂单资讯到 beans_TaskRecordList
                         *
                         */
                        def UserTaskList = new beans_TaskRecordList()
                        UserTaskList.with {
                            setTaskList_UserID(userInfo.getUserid())
                            setTaskList_UserName(userInfo.getUsername())
                            setTaskList_gameCategory(userInfo.getGameCategory())
                            setTaskList_gameType(userInfo.getGameType())
                            setTaskList_StartTime(userInfo.getStartTime())
                            setTaskList_EndTime(userInfo.getEndTime())
                        }

                        AnalysisBetNum analysisbetInfo = AnalysisBetNum.instance
                        UserTaskList = analysisbetInfo.initData(userInfo, openDrawInfo, UserTaskList)
                        AllUserTaskInfo.put(userID, UserTaskList)
                    }else{
                        def UserTaskList = AllUserTaskInfo.get(userID)
                        AnalysisBetNum analysisbetInfo = AnalysisBetNum.instance
                        UserTaskList = analysisbetInfo.initData(userInfo, openDrawInfo, UserTaskList)
                        AllUserTaskInfo.put(userID, UserTaskList)
                    }


                }
            }
        }
    }
}
