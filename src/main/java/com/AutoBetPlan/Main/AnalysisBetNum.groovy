package com.AutoBetPlan.Main

import com.AutoBetPlan.GameMethodAnalysis.Enum_BallGame_Define
import com.AutoBetPlan.GameMethodAnalysis.Enum_BallGame_SSC
import com.AutoBetPlan.beans.API_BetPlanInfo
import com.AutoBetPlan.beans.API_OpenDraw
import com.AutoBetPlan.beans.beans_TaskRecordTask
import com.AutoBetPlan.beans.beans_TaskRecord_BetInfo
import com.AutoBetPlan.beans.beans_TaskRecordList

/**
 * Created by cjay on 2017/3/21.
 */
@Singleton
class AnalysisBetNum {

    /***
     * 添加 方案 并 记录分析 目前方案的投注内容
     * @param betPlanInfo
     * @param IssueInfo
     * @param UserBetInfo
     * @return
     */
    def initData(API_BetPlanInfo betPlanInfo, API_OpenDraw IssueInfo, beans_TaskRecordList UserBetInfo) {

        switch (Enum_BallGame_Define.getEnumNameByCode(betPlanInfo.getGameCategory())) {
            case Enum_BallGame_Define.GAME_EVERYHOURS:

                if (!UserBetInfo.TaskList_ItemInfoList) {
                    /***
                     * 当取不到 挂机 注单 记录 时 先同步 全部新增一次
                     * 代表该注单 第一次新增
                     */
                    List<API_BetPlanInfo.AutoPlanDataBean> autoPlanList = betPlanInfo.getAutoPlanData()
                    UserBetInfo.TaskList_ItemInfoList = new HashMap<String,beans_TaskRecordTask>()

                    for (int i = 0; i < autoPlanList.size(); i++) {
                        def singalPlan = autoPlanList.get(i)
                        /***
                         * 这边是 先把用户所有的挂机 注单 加上随机号后 写入自己的记录
                         * 用CODE 就可以核对 目前要更新哪一个任务
                         */
                        String Code = generator((('A'..'Z') + ('0'..'9')).join(), 9)
                        def newTask = new beans_TaskRecordTask()
                        def newBetRecord = new beans_TaskRecord_BetInfo()
                        newBetRecord.with {
                            setIsUpdate(false)
                            setBrokerage(0)
                            setGame("")
                            setMethod("")
                            setOriginalNumbers("")
                            setMulti(0)
                            setMode(0)
                            setPeriod("")
                        }
                        newTask.setTaskItem_betInfo(newBetRecord)
                        newTask.with {
                            setTaskItem_Name(singalPlan.getName())
                            setTaskItem_Code(Code)
                            setTaskItem_Type(singalPlan.getType())
                            setTaskItem_MethodCode(singalPlan.getMethodCode())
                            setTaskItem_MethodContent(singalPlan.getContent())
                            setTaskItem_Mode(singalPlan.getMode())
                            setTaskItem_Multi(singalPlan.getMulti())

                            setTaskItem_WON(0)
                            setTaskItem_LOSE(0)
                        }
                        UserBetInfo.TaskList_ItemInfoList.put(Code, newTask)
                    }
                }

                /***
                 * 当里面有记录时，只要更新 当前任务内的纪录而已
                 */
                Enum_BallGame_SSC ssc = Enum_BallGame_SSC.instance

                String BetType = UserBetInfo.getTaskList_gameType()

                UserBetInfo.TaskList_ItemInfoList.each { TaskCode, CurrentTask ->
                    ssc.initData(BetType, IssueInfo, CurrentTask)
                    CurrentTask.TaskItem_betInfo = ssc.getBetInfoInCurrentTask()
                    CurrentTask.TaskList_BetTimes+=1//已 计算 后要加一
                }

                return UserBetInfo
            case Enum_BallGame_Define.GAME_11PICK5:
                break
            case Enum_BallGame_Define.GAME_3DP3:
                break
            case Enum_BallGame_Define.GAME_K3:
                break
            case Enum_BallGame_Define.GAME_KLSF:
                break
            case Enum_BallGame_Define.GAME_PK10:
                break
            case Enum_BallGame_Define.GAME_KENO:
                break
            case Enum_BallGame_Define.GAME_BETTING:
                break
        }
    }

    def generator = { String alphabet, int n ->
        new Random().with {
            (1..n).collect { alphabet[nextInt(alphabet.length())] }.join()
        }
    }


}
