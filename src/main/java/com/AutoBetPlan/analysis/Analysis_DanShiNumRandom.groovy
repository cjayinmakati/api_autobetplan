package com.AutoBetPlan.analysis
/**
 * Created by cjay on 2017/3/22.
 */
class Analysis_DanShiNumRandom {

    static void main(String[] args) {
        Analysis_DanShiNumRandom rand= new Analysis_DanShiNumRandom();
        println(rand.DanShiRandom(10,3))
    }

    /***
     * 计算 单式 随机注数
     * @param TotalAmount 需求单式数量 900期
     * @param LevelAmount 几星 4星单式
     * @return
     */
    String DanShiRandom(int TotalAmount, int LevelAmount) {
        int max = 10
        String formater = ""
        switch (LevelAmount) {
            case 2:
                max = 99
                formater = "%02d"
                break
            case 3:
                max = 999
                formater = "%03d"
                break
            case 4:
                max = 9999
                formater = "%04d"
                break
            case 5:
                max = 99999
                formater = "%05d"
                break
        }
        Random rand = new Random()
        List<String> array = new ArrayList<>()
        while (array.size() < TotalAmount) {
            String.format("%09d", 123)
            String randomNum = String.format(formater,rand.nextInt(max + 1))
            if (!array.contains(randomNum)) {
                array << randomNum
            }
        }

        return String.join(" ", array.sort())
    }
}
